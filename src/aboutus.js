import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Dimensions, Text, Image, TouchableOpacity, BackHandler} from 'react-native';
import { Actions } from 'react-native-router-flux';


class aboutus extends Component {

  

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton(){
    Actions.home()
    return true;
}

  componentWillMount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
  }

 
  render() {
    return (
        <View style={styles.main}>
        <View style={styles.topBar}>
        <Text style={styles.headbarText}> About US </Text>
        </View>
        <View style={{alignItems:'center', justifyContent:'center', width:'100%', height:'90%'}}>
        <Text style={{textAlign:'center'}}>
        Karachi Automobile Services is a one stop solution for all your car repair and
Services need. Our mission is to provide trusted car repair and maintenance in
most convenient way.it only takes seconds for you to register with KAMS and
being your car care journey with us. We take care of all your car needs.

KAMS offers on demand car wash, repair and other services based on the customer
preferences. You can search for providers based on your location and type of
problem and them so they could help you by callings us you can reliable and
quick services.

        </Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    main:{
        height:'100%',
        width:'100%',
    },
    topBar:{
      height:'10%',
      backgroundColor:'#04005b',
      width:'100%',
    },
    touch:{
      paddingTop:'4%',
      paddingLeft:10,
      width:'10%'
    },
    headbarText:{
        width:'100%',
        color:'silver',
        fontWeight:'400',
        fontSize:20,
        textAlign:'center',
        paddingTop:'4%',
    },
  container: {
    height: '90%',
    width: '100%',
  }
});

export default aboutus;