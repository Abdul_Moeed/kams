import React, {Component} from 'react';
import {View, Text, AsyncStorage, StyleSheet, BackHandler, TouchableOpacity, Image, FlatList,} from 'react-native';
import Communications from 'react-native-communications'
import firebaseaction from './firebase';
import {Actions} from 'react-native-router-flux';
import {Spinner} from './components';




class list extends Component{
    constructor(){
        super();
    this.state ={
        category:'',
        fbData:[],
        loading:true,

    }
}



    componentDidMount(){
        AsyncStorage.getItem('category').then((x)=>{
            this.setState({category:x})
            const root = firebaseaction.database().ref()
            const data = root.child(this.state.category)
            data.on('value', (snap)=>{
                const fbData = [];
                snap.forEach((dat)=>{
                    let result = dat.val()
                    fbData.push(result);
                    console.log('Dekh Lo', fbData)
                    this.setState({
                        fbData: fbData,
                    })
                })
            })
        })
        // this.getItems(this.itemRef);
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        // this.getItems(this.itemRef);
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton(){
        Actions.category()
           }

    render(){
        return(
            <View style={styles.container}>
            <View style={styles.headbar}>
            <Text style={styles.headbarText}>{this.state.category}</Text>
            </View>
                  <FlatList 
                  data = {this.state.fbData}
                  renderItem={({item, index})=>{
                      return(
                        <View style={styles.listView}>
                <TouchableOpacity
                onPress={()=>{
                    Actions.address()
                    AsyncStorage.setItem('lat', item.place_latitue)
                    AsyncStorage.setItem('long', item.place_Logitute)
                }
                }
                style={styles.addressView}>
                <Image style={styles.addressImage} source={require('./assets/mapicon.png')}/>
                    <Text style={styles.addressText}>{item.place_name}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.contactView} onPress={() => Communications.phonecall(item.PhoneNumber, true)}>
                <Image style={styles.addressImage} source={require('./assets/phone.png')}/>
                    <Text style={styles.addressText}>{item.PhoneNumber}</Text>
                    </TouchableOpacity>
            </View>
                      )
                  }}
                  >

                  </FlatList>
            
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container:{
        backgroundColor:'#ededed',
        height:'100%',
        width:'100%',
        alignItems:'center'
    },
    headbar:{
        width:'100%',
        backgroundColor:'#04005b',
        height:50,
        marginBottom:5,

    },
    headbarText:{
        fontSize:30,
        textAlign:'center',
        paddingTop:5,
        color:'white',
        fontWeight:'400'
    },
    listView:{
        height:130,
        width:'95%',
        borderWidth:2,
        borderColor:'#04005b',
        marginBottom:5,
    },
    addressView:{
        flexDirection:'row',
        height:60,
        alignItems:'center'
    },
    addressImage:{
        width:50,
        height:50,
        marginLeft:10,
        marginRight:10
    },
    addressText:{
        width:'80%',
        color:'#04005b',
        fontSize:16,
        fontWeight:'bold'
    },
    contactView:{
        height:60,
        flexDirection:'row',
        alignItems:'center'
    }
})


export default list;