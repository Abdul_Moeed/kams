import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Dimensions, Text, Image, TouchableOpacity, BackHandler, AsyncStorage, ToastAndroid, Modal, TouchableHighlight} from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import {Actions} from 'react-native-router-flux';
import Polyline from '@mapbox/polyline';
import Communications from 'react-native-communications'
import firebaseaction from './firebase';
import {Spinner} from './components';


let { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 24.868224;
const LONGITUDE = 67.006123;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class address extends Component{
    constructor() {
        super();
        this.state = {
          region: {
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          },
          dropLatitude:null,
          dropLongitude:null,
          pickLatitude:null,
          pickLongitude:null,
          coords:[],
          category:'',
          fbData:[],
          dropLoc:[],
          modalVisible: false,
          yLocation:'',
          dLocation:'',
          sName:'',
          sPhone:'',
          sDistance:'',
          eTime:'',
          loading:false
        };
      }
      toggleModal(visible){
        this.setState({modalVisible: visible})
      }

      pickLocation(){
        return(
        <MapView.Marker pinColor = 'purple' coordinate={this.state.region}>
        </MapView.Marker>
        )
      }

      // RenderMapMarker(){
      //   this.state.fbData.map((x)=> {
      //     return(
      //       <MapView.Marker  coordinate={{
      //         latitude : Number(x.place_latitue),
      //         longitude : Number(x.place_Logitute)
      //       }}
            
      //       />
      //     )
      //   })
      // }

    //   componentWillReceiveProps(){
    //     this.state.fbData.map((x)=> {
    //       return(
    //         this.setState({
    //           dropLatitude:x.place_latitue,
    //           dropLatitude:x.place_Logitute,
    //           pickLatitude:this.state.latitude,
    //           pickLongitude:this.state.longitude
    //         })
    //       )
    // })
    //   }

      componentDidUpdate(){

        
        
        let pickLt = this.state.pickLatitude
        let pickLng = this.state.pickLongitude
        let pickLoc = String([pickLt,pickLng])
        let dropLt =  this.state.dropLatitude
        let dropLng = this.state.dropLongitude
        let dropLoc = String([dropLt,dropLng])
        this.getDirections(pickLoc,dropLoc)
      }

      async getDirections(startLoc, destinationLoc) {
        try {
          let mode = 'driving';
          let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }&mode=${mode}`)
          let respJson = await resp.json();
          let distanceCover = respJson.routes[0].legs[0].distance.text;
          let yourLocation = respJson.routes[0].legs[0].start_address;
          let shopLocation = respJson.routes[0].legs[0].end_address;
          let estimatedTime = respJson.routes[0].legs[0].duration.text;
          let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
          let coords = points.map((point, index)=>{
            return {
              latitude : point[0],
              longitude : point[1]
            };
          });
          this.setState({
            coords:coords,
            yLocation:yourLocation,
            dLocation: shopLocation,
            sDistance:distanceCover,
            eTime:estimatedTime
          });
          return coords        
        } catch(error){
          return(error)
        }
      }

      closeSpinner=()=>setTimeout(()=> this.setState({loading:false}),5000)

      
      componentDidMount() {

    
        AsyncStorage.getItem('category').then((x)=>{
          this.setState({category:x})
          const root = firebaseaction.database().ref()
          const data = root.child(this.state.category)
          data.on('value', (snap)=>{
              const fbData = [];
              snap.forEach((dat)=>{
                  let result = dat.val()
                  fbData.push(result);
                  this.setState({
                      fbData: fbData,
                  })
              })
          })
      })

      //   AsyncStorage.getItem('lat').then((item)=>{
      //     let dropLat = Number(item)
      //     this.setState({dropLatitude:dropLat})
      // })
      // AsyncStorage.getItem('long').then((item)=>{
      //     let dropLong = Number(item)
      //     this.setState({dropLongitude:dropLong})
      // })

        
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        navigator.geolocation.getCurrentPosition(
          position => {
            this.setState({
              region: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              },
              pickLatitude:position.coords.latitude,
              pickLongitude:position.coords.longitude
            });
          },
        (error) => ToastAndroid.show(error.message, ToastAndroid.SHORT),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
        this.watchID = navigator.geolocation.watchPosition(
          position => {
            this.setState({
              region: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              },
              pickLatitude:position.coords.latitude,
              pickLongitude:position.coords.longitude
            });
          }
        );
      }

      componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
      }

      handleBackButton(){
        Actions.category()
        return true;
    }

    renderView(){
      if(this.state.loading === false){
        return(
          <View style={modal.info}>
        <Text style={modal.txt}>Place Name: {this.state.sName}</Text>            
          <Text style={modal.txt}>Your Location: {this.state.yLocation}</Text>
          <Text style={modal.txt}>End Location: {this.state.dLocation}</Text>
          <Text style={modal.txt} onPress={()=> Communications.phonecall(this.state.sPhone, true)}>Contact Number: {this.state.sPhone}</Text>
          <Text style={modal.txt}>Distance: {this.state.sDistance}</Text>
          <Text style={modal.txt}>Estimated Time: {this.state.eTime}</Text>
        </View>
    )
      }
      else{
        return <Spinner/>
      }
    }

    Region(){
      return(
        <MapView
        provider={ PROVIDER_GOOGLE }
        style={ styles.container }
        showsUserLocation={ true }
        region={ this.state.region }
        zoomEnabled={true}
      >
        {this.pickLocation()}
        {/* {this.dropLocation()} */}
        
        <MapView.Polyline
        coordinates={this.state.coords}
        strokeWidth={5}
        strokeColor="#032f53" />
        
        {/* {this.RenderMapMarker()} */}
        {this.state.fbData.map((x, index)=> {
          return(
            <MapView.Marker key={index} coordinate={{
              latitude : Number(x.place_latitue),
              longitude : Number(x.place_Logitute)}}
              onPress={()=>{
                this.setState({
                  dropLatitude:Number(x.place_latitue),
                  dropLongitude:Number(x.place_Logitute),
                  sName:x.place_name,
                  sPhone:x.PhoneNumber
                })
                {this.toggleModal(true)}
                {this.setState({loading:true})}
                this.closeSpinner()
              }}
              />
          )
        })}

      </MapView>
      )
    }

    render() {
      {this.pickLocation()}
      // {this.RenderMapMarker()}
      // {this.dropLocation()}
        return(
          <View style={styles.main}>
          <View style={styles.headbar}>
          <Modal animationType = {"fade"} transparent = {true}
        visible = {this.state.modalVisible}>
        <View style={modal.container}>
        <TouchableHighlight style={modal.top} onPress={() => {this.toggleModal(!this.state.modalVisible)}}>
        <Image source={require('./assets/cross.png')} style={modal.img}/>
        </TouchableHighlight>
        {this.renderView()}
        </View>
        </Modal>
          <Text style={styles.txt}>{this.state.category}</Text>
          </View>
          {this.Region()}
      </View>
        )
    }
    
}

const styles = StyleSheet.create({
    main:{
    flex: 1,
    },
    container:{
        height:'100%',
        width:'100%'
    },
    headbar:{
      height:'10%',
      width:'100%',
      backgroundColor:'#032f53',
      alignItems:'center',
      justifyContent:'center'
    },
    txt:{
      fontWeight:'bold',
      fontSize:22,
      color:'white'
    }
})

const modal = StyleSheet.create({
  container:{
      flex: 3,
      alignItems: 'center',
      borderRadius:10,
      backgroundColor: '#dbdfff',
      margin:'6%'
  },
  top:{
      width:'100%',
      height:'10%',
      justifyContent:'center',
      alignItems:'flex-end'
  },
  img:{
      height:40,
      width:40
  },
  info:{
      height:'80%',
      justifyContent:'center',
      borderColor:'#ced4ff',
      borderRadius:10,
      borderWidth:2
  },
  txt:{
      fontSize:16,
      margin:10,
      color:'black'
  },
  buttons:{
      height:'15%',
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center'
  }
})



export default address