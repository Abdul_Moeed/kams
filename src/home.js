import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Dimensions, Text, Image, TouchableOpacity, BackHandler} from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import DrawerLayout from 'react-native-drawer-layout'
import Sidemenu from './components/sidemenu'

let { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class home extends Component {
  constructor() {
    super();
    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      }
    };
  }

  

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }
        });
      },
    (error) => alert(error.message),
    { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
    this.watchID = navigator.geolocation.watchPosition(
      position => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }
        });
      }
    );
  }

  handleBackButton(){
    BackHandler.exitApp()
    return true;
}

  componentWillMount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }
  render() {
    const sidemenu = <Sidemenu/>;
    return (
      
      <DrawerLayout
      drawerWidth={220}
      ref={(drawer)=>{return this.drawer = drawer}}
      keyboardDismissMode='on-drag'
      renderNavigationView={()=> sidemenu}>

        <View style={styles.main}>
        <View style={styles.topBar}>
       <TouchableOpacity style={styles.touch} onPress={() => this.drawer.openDrawer()}>
        <Image source={require('./assets/menu.png')}
        style={styles.menuimage}
        />
        </TouchableOpacity>
        <Text style={styles.headbarText}> Nearby Places </Text>
        </View>
      <MapView
        provider={ PROVIDER_GOOGLE }
        style={ styles.container }
        showsUserLocation={ true }
        region={ this.state.region }
      >
        <MapView.Marker
          coordinate={ this.state.region }
        />
      </MapView>
      </View>
      </DrawerLayout>
    );
  }
}
const styles = StyleSheet.create({
    main:{
        height:'100%',
        width:'100%',
    },
    topBar:{
      flexDirection:'row',
      height:'10%',
      backgroundColor:'#04005b',
      width:'100%',
    },
    touch:{
      paddingTop:'4%',
      paddingLeft:10,
      width:'10%'
    },
    menuimage:{
      width:30,
      height:30
    },
    headbarText:{
        width:'90%',
        color:'silver',
        fontWeight:'400',
        fontSize:20,
        textAlign:'center',
        paddingTop:'4%',
    },
  container: {
    height: '90%',
    width: '100%',
  }
});

export default home;