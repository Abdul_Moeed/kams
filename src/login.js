import React, { Component } from 'react';
import {View, Text, StyleSheet, TextInput, TouchableOpacity, ActivityIndicator, Button, BackHandler} from 'react-native';
import firebaseaction from './firebase'
import {Actions} from 'react-native-router-flux'
import {Spinner} from './components'



class login extends Component {
    state={email:'' , password:'', error:'', loading: false}

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton(){
        Actions.landing()
        return true;
    }

    onButtonPress(){
        const{email,password} = this.state;
        this.setState({error:'', loading: true});

        firebaseaction.auth().signInWithEmailAndPassword(email, password)
        .then(this.onLoginSuccess.bind(this))
        .catch(this.onLoginFail.bind(this))
    }

    onLoginFail(){
        this.setState({error:'Authentication Failed', loading:false})
        alert('Authentication Failed')
    }


    onLoginSuccess(){
        this.setState({
            email:'',
            password:'',
            error:'',
            loading:false

        })
        Actions.home()

    }

    renderButton(){
        
        if(this.state.loading){
            return <Spinner size="small"/>;
        }

        return(
            <TouchableOpacity
                    onPress={()=>this.onButtonPress()}
                    style={styles.button}>
                    <Text style = {styles.buttonText}> Login </Text>
                    </TouchableOpacity>
        );
        
    }

    

    render(){
        return(
            <View style={styles.container}>
            <View style={styles.box1}>
                <Text style={styles.headbarText}>Login</Text>
                </View>
                <View style={styles.box2}>
                <TextInput
                placeholder='Please Input Email'
                placeholderTextColor='black'
                keyboardType='email-address'
                onChangeText={email=> this.setState({email})}
                style={styles.formInput}/>

                <TextInput
                placeholder='Please Input Password'
                placeholderTextColor='black'
                secureTextEntry={true}
                onChangeText={password=> this.setState({password})}
                style={styles.formInput}/>
                
                    </View>
                    <View style={styles.box3}>
                    {this.renderButton()}
                    </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:3,
        flexDirection:'column',
        backgroundColor:'#ededed',
        height:'100%',
        width:'100%'
    },
    box1:{
        height:'20%',
    },
    box2:{
        height:'40%',
        alignItems:'center'
    },
    box3:{
        height:'40%',
        justifyContent:'center',
        alignItems:'center'
    },
    headbarText:{
        backgroundColor:'#04005b',
        fontSize:30,
        height:50,
        textAlign:'center',
        paddingTop:5,
        color:'white',
        fontWeight:'400'
    },
    form1:{
        width:'40%',
        alignItems:'flex-end',
        justifyContent:'center'
    },
    formText:{
        paddingTop:10,
        color:'gray',
        fontSize:20,
    },
    form2:{
        justifyContent:'center',
        alignItems:'flex-end',
        paddingRight:50,
    },
    formInput:{
        width:'90%',
        alignItems:'center'
    },
    button:{
        borderColor:'#04005b',
        width:'95%',
        height:50,
        borderRadius:5,
        backgroundColor:'silver',
        borderWidth:1,
        justifyContent:'center'
    },
    buttonText:{
        textAlign:'center',
        fontSize:20,
        color:'#04005b'
    },
    
})
    


export default login;