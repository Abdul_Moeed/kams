import React from 'react';
import {Router, Scene} from 'react-native-router-flux';


import splashScreen from './splashScreen';
import landing from './landing';
import signup from './signup';
import login from './login';
import home from './home';
import category from './category';
// import list from './list';
import address from './address';


const routes = () =>(
    <Router>
        <Scene key='root'>
        <Scene key='splashscreen' component = {splashScreen} hideNavBar={true} initial={true}/>
        <Scene key='landing' component = {landing} hideNavBar={true}/>
        <Scene key='signup' component = {signup} hideNavBar={true}/>
        <Scene key='login' component = {login} hideNavBar={true}/>
        <Scene key='home' component = {home} hideNavBar={true}/>
        <Scene key='category' component={category} hideNavBar={true} />
        {/* <Scene key='list' component={list} hideNavBar={true} /> */}
        <Scene key='address' component={address} hideNavBar={true} />

        </Scene>
    </Router>
)

export default routes;