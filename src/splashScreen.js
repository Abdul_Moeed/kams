import React, { Component } from 'react';
import { View, StyleSheet, Text, ImageBackground, Image} from 'react-native';
import {Actions} from 'react-native-router-flux'
import firebaseaction from './firebase'
import {Spinner} from './components'


class splashScreen extends Component{
    state = {loading:true, loggedIn:false};


    closeSplash=()=>setTimeout(()=>this.renderContent(),3000)

componentWillMount(){
    firebaseaction.auth().onAuthStateChanged((user)=>{
        if (user) {
            this.setState({loggedIn: true});
        } else {
            this.setState({loggedIn:false});
        }
    });
}

renderContent(){
    if (this.state.loggedIn){
        return Actions.home()
    } else { return Actions.landing()}
}


componentDidMount(){
    this.closeSplash()
}

navigate(){
    <login/>
}

render(){
    return(
        <View style={{alignItems:'center', justifyContent:'center', width:'100%', height:'100%'}}>
        <Text style={{backgroundColor:'transparent', fontWeight:'bold', color:'grey', fontSize:20}}> Karachi AutoMobile Services </Text>
        <Image source= {require('./assets/logo.png')}/>
        </View>
    )

}
}

export default splashScreen;