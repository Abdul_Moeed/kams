import React, { Component } from 'react';
import {View, Text, StyleSheet, TextInput, TouchableOpacity, Image} from 'react-native';
import firebaseaction from './firebase'
import {Actions} from 'react-native-router-flux'


class landing extends Component {

    

    render(){
        return(
            <View style={styles.container}>
            <View style={styles.box1}>
                <Text style={styles.headbarText}>Welcome</Text>
                <Image source={require('./assets/logo.png')}
                style={styles.image}/>
                </View>
                <View style={styles.box2}>

                <TouchableOpacity
                    onPress={()=>Actions.signup()}
                    style={styles.button}>
                    <Text style={styles.buttonText}>SignUp</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                    onPress={()=>Actions.login()}
                    style={styles.button}>
                    <Text style={styles.buttonText}>Login</Text>
                    </TouchableOpacity>
               
                    </View>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:3,
        flexDirection:'column',
        backgroundColor:'#ededed',
        height:'100%',
        width:'100%'
    },
    box1:{
        height:'40%',
    },
    box2:{
        height:'40%',
        alignItems:'center'
    },
    
    headbarText:{
        width:'100%',
        backgroundColor:'#04005b',
        fontSize:30,
        height:50,
        textAlign:'center',
        paddingTop:5,
        color:'white',
        fontWeight:'400'
    },
    form1:{
        width:'40%',
        alignItems:'flex-end',
        justifyContent:'center'
    },
    formText:{
        paddingTop:10,
        color:'gray',
        fontSize:20,
    },
    form2:{
        justifyContent:'center',
        alignItems:'flex-end',
        paddingRight:50,
    },
    formInput:{
        width:'90%',
        alignItems:'center'
    },
    button:{
        borderColor:'#04005b',
        width:'95%',
        height:50,
        borderRadius:5,
        backgroundColor:'silver',
        borderWidth:1,
        justifyContent:'center',
        marginTop:20,
    },
    buttonText:{
        textAlign:'center',
        fontSize:20,
        color:'#04005b'
    },
    image:{
        marginLeft:'25%',
        width:200,
        height:200
    }


})
    


export default landing;