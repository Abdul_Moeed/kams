import React,{Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import firebaseaction from '../firebase';
import {Actions} from 'react-native-router-flux';
import Communications from 'react-native-communications'


class Sidemenu extends Component{
    render(){
        return(
            <View style={styles.container}>
            <View style={styles.imageContainer}>
            <Image
            source={require('../assets/logo.png')}
            style={styles.image}/>
            <Text style={styles.name}>Karachi AutoMobile Services</Text>
            </View>
            <View style={styles.list}>
            <TouchableOpacity
            onPress={()=>Actions.category()}
            style={styles.option}>
            <Text style={styles.text}>Category</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.option} onPress={()=> Actions.aboutus()}>
            <Text style={styles.text}>About Us</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.option} onPress={() => Communications.email('kams76953@gmail.com', null,null,'Customer Care','')} >
            <Text style={styles.text}>Write mail to us</Text>
                </TouchableOpacity>
                <TouchableOpacity
                onPress={()=> firebaseaction.auth().signOut() && Actions.landing()}
                style={styles.option}>
            <Text style={styles.text}>Logout</Text>
                </TouchableOpacity>

            </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        width:'100%',
        height:'100%',
        backgroundColor:'white',
        justifyContent:'flex-start',
        alignItems:'center'
    },
    imageContainer:{
        height:'30%',
        width:220,        
        backgroundColor:'#4286f4',
    },
    image:{
        height:100,
        width:100
    },
    name:{
        fontSize:20,
        marginLeft:10
    },
    text:{
        fontSize:16,
        fontWeight:'400'
    },
    option:{
        height:40,
        width:200,
        justifyContent:'center',
        margin:5,
    }
})

export default Sidemenu;