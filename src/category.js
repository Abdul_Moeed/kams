import React, { Component } from 'react';
import {View, Text, StyleSheet, TextInput, TouchableOpacity, Image, BackHandler, ScrollView, AsyncStorage} from 'react-native';
import {Actions} from 'react-native-router-flux';

class category extends Component {

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton(){
        Actions.home()
        return true;
    }

    

            render(){
                return(
                    <View style={styles.container}>
                    <View style={styles.box1}>
                        <Text style={styles.headbarText}>Whats Your Problem?</Text>
                        </View>

                        <View style={styles.subContainer}>
                        <View style={styles.rowPortion}>
                        <TouchableOpacity onPress={()=>Actions.address() || AsyncStorage.setItem('category', 'Tyre Shop')} style={styles.leftBox}>
                        <Image style={styles.image1}source={require('./assets/tyreshop.png')}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>Actions.address() || AsyncStorage.setItem('category', 'Battery')} style={styles.rightBox}>
                        <Image style={styles.image1} source={require('./assets/battery.png')}/>
                        </TouchableOpacity>
                            </View>
                            <View style={styles.rowPortion}>
                        <TouchableOpacity onPress={()=>Actions.address() || AsyncStorage.setItem('category', 'Gas Station')} style={styles.leftBox}>
                        <Image style={styles.image1} source={require('./assets/pump.png')}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>Actions.address() || AsyncStorage.setItem('category', 'BreakDownn')} style={styles.rightBox}>
                        <Image style={styles.image1}source={require('./assets/breakdown.png')}/>
                        </TouchableOpacity>

                            </View>

                            <TouchableOpacity onPress={()=>Actions.address() || AsyncStorage.setItem('category', 'Rescue')} style={styles.lastBox}>
                            <Image style={styles.image1} source={require('./assets/rescue.png')}/>
                                </TouchableOpacity>

                        </View>
                        
                    </View>
                );
            }
        }
        
        const styles = StyleSheet.create({
            container:{
                flex:3,
                flexDirection:'column',
                backgroundColor:'#ededed',
                height:'100%',
                width:'100%'
            },
            headbarText:{
                backgroundColor:'#04005b',
                fontSize:30,
                height:50,
                textAlign:'center',
                paddingTop:5,
                color:'white',
                fontWeight:'400'
            },
            subContainer:{
                width: '100%',
                height: '100%',
                backgroundColor:'#dcdbff',
                flex:3,
                justifyContent:'center',
                alignItems:'center'
            },
            rowPortion:{
                flexDirection:'row',
                height:'33%',
                justifyContent:'center',
                alignItems:'center'
            },
            leftBox:{
                width:'40%',
                height:'80%',
                justifyContent:'center',
                alignItems:'center',
                borderWidth:3,
                borderColor:'#04005b',
                marginRight:5
            },
            rightBox:{
                width:'40%',
                height:'80%',
                justifyContent:'center',
                alignItems:'center',
                borderWidth:3,
                borderColor:'#04005b',
                marginLeft:5
            },
            lastBox:{
                height:'25%',
                width:'70%',
                marginTop:'5%',
                justifyContent:'center',
                alignItems:'center',
                borderWidth:3,
                borderColor:'#04005b'

            },
            image1:{
                width: 100,
                height: 100
            }
           
            
        })
    


export default category;