import { AppRegistry } from 'react-native';
import routes from './src/routes';

console.disableYellowBox = true

AppRegistry.registerComponent('KAMS', () => routes);
